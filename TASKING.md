### 2.1 默认行为
1. should return 500 when not throw exception.
### 2.2 Controller Level Handler
1. should return 500 when throw exception.
### 2.3 Controller Level Handler with Multiple Exception Types
1. should return 418 when get an null pointer exception.
1. should return 418 when get an arithmetic exception.
### 2.4 Controller Advicer
1. should return 418 when handler the exception by the global controller.
