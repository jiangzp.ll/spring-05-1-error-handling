package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_500_when_not_throw_exception() throws Exception {
        ResponseEntity<Void> entity =testRestTemplate.getForEntity("/api/errors/default", Void.class);
        assertEquals(500, entity.getStatusCodeValue());
    }

    @Test
    void should_return_500_when_throw_exception() {
        ResponseEntity<String> entity =testRestTemplate.getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals(500, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody().toString());
    }

    @Test
    void should_return_418_when_get_an_null_pointer_exception() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody());
    }

    @Test
    void should_return_418_when_get_an_arithmetic_exception() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/errors/arithmetic", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument", entity.getBody());

    }

    @Test
    void should_return_418_when_handler_the_exception_by_the_global_controller() {
        ResponseEntity<String> entity;

        entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with brother or sister", entity.getBody());

        entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(418, entity.getStatusCodeValue());
        assertEquals("Something wrong with brother or sister", entity.getBody());
    }
}
