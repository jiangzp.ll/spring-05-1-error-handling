package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class UserController {

    @GetMapping("/errors/default")
    void getDefaultException() {
        throw new RuntimeException();
    }

    @GetMapping("/errors/illegal-argument")
    public void getException() {
        throw new RuntimeException("This is a exception");
    }

    @GetMapping("/errors/null-pointer")
    public void getNullPointerException() {
        throw new NullPointerException();
    }

    @GetMapping("/errors/arithmetic")
    public void getArithmeticException() {
        throw new ArithmeticException();
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> exceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Something wrong with the argument");
    }

    @ExceptionHandler({ NullPointerException.class, ArithmeticException.class })
    public ResponseEntity<String> handleException() {
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT)
                .body("Something wrong with the argument");
    }
}
