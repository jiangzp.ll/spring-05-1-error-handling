package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @GetMapping("/api/sister-errors/illegal-argument")
    public void getIllegalArgumentExceptionSister() {
        throw new IllegalArgumentException();
    }


    @GetMapping("/api/brother-errors/illegal-argument")
    public void getIllegalArgumentExceptionBorder() {
        throw new IllegalArgumentException();
    }

}
